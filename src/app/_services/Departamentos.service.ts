import { Injectable, Inject } from '@angular/core'
import { Observable } from 'rxjs'
import { HttpClient, HttpHeaders, HttpParams, JsonpClientBackend } from '@angular/common/http'
import { map, filter, switchMap, catchError } from 'rxjs/operators'
import { getBaseUrl } from '../app.config'
import Swal from 'sweetalert2'
import { NgxSpinnerService } from 'ngx-spinner'
import { DepartamentosModelo } from '../_models'

@Injectable({
	providedIn: 'root'
})

export class DepartamentosService {

	private urlEndPoint: string = getBaseUrl()
	private httpOptionsPost = {
		headers: new HttpHeaders({
		  "Content-Type": "application/json"
		})
	}
	private httpOptionsPostFile = {
		headers: new HttpHeaders({
		  "Content-Type": "application/x-www-form-urlencoded"
		})
	}
	
	constructor(
		private _http: HttpClient,
		private _Spinner : NgxSpinnerService
	) { }

	async mostrarTodos()
	{
		try {
			const res = await this._http
						.get<DepartamentosModelo[]>( this.urlEndPoint + 'departamentos/mostrarTodos' )
						.toPromise()
						.then(resp => {
							return resp
						},
						pError => {
							Swal.fire(pError.statusText, pError.message, 'error')
							this._Spinner.hide()
						})
			const data = <DepartamentosModelo[]>res
			return data

		} catch (error) {
			console.log(error)
		}
	}

	async mostrarUno(id: number)
	{
		try {
			const res = await this._http
						.get<DepartamentosModelo[]>( this.urlEndPoint + 'departamentos/mostrarUno/' + id )
						.toPromise()
						.then(resp => {
							return resp
						},
						pError => {
							Swal.fire(pError.statusText, pError.message, 'error')
							this._Spinner.hide()
						})
			const data = <DepartamentosModelo[]>res
			return data

		} catch (error) {
			console.log(error)
		}
	}

	async mostrarPorPais(idPais:number=1)
	{
		try {
			const res = await this._http
						.get<DepartamentosModelo[]>( this.urlEndPoint + 'departamentos/mostrarPorPais/' + idPais )
						.toPromise()
						.then(resp => {
							return resp
						},
						pError => {
							Swal.fire(pError.statusText, pError.message, 'error')
							this._Spinner.hide()
						})
			const data = <DepartamentosModelo[]>res
			return data

		} catch (error) {
			console.log(error)
		}
	}

	async crear(dataSend: DepartamentosModelo)
	{
		try {
			const res = await this._http
						.post<DepartamentosModelo>( this.urlEndPoint + 'departamentos/crear', JSON.stringify(dataSend), this.httpOptionsPost )
						.toPromise()
						.then(resp => {
							return resp
						},
						pError => {
							console.log(pError)
							Swal.fire('Error', pError.message, 'error')
							this._Spinner.hide()
						})
			const data = <DepartamentosModelo>res
			return data

		} catch (error) {
			console.log(error)
			Swal.fire('Error Catch', error, 'error')
		}
	}

	async actualizar(dataSend: DepartamentosModelo)
	{
		try {
			const res = await this._http
						.post<DepartamentosModelo>( this.urlEndPoint + 'departamentos/actualizar', dataSend, this.httpOptionsPost )
						.toPromise()
						.then(resp => {
							return resp
						},
						pError => {
							Swal.fire(pError.statusText, pError.message, 'error')
							this._Spinner.hide()
						})
			const data = <DepartamentosModelo>res
			return data

		} catch (error) {
			console.log(error)
		}
	}
}
