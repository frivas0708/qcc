import { Injectable, EventEmitter } from '@angular/core';
import { Subscription } from 'rxjs/internal/Subscription';

@Injectable({
	providedIn: 'root'
})
export class EventEmitterService
{
	initFunction = new EventEmitter();    
	subsVar: Subscription;    
		
	constructor() { }    
		
	initFunctionEmmitter() {					// To pass data add obj --> getTaskEmmitter(data:string)
		this.initFunction.emit();	// Add obj to emit --> this.getTasksFunction.emit(data)
	}
}
