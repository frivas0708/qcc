import { Injectable } from '@angular/core';
import * as m from 'moment';


@Injectable({
	providedIn: 'root'
})
export class FnService {

	private simpleFormat = /^\d{2}\/\d{2}\/\d{4}$/

	constructor(){}

	formatDate(date: Date)
	{
		return date.getFullYear() + '-' + (date.getMonth()+1).toString().padStart(2,'00') + '-' + date.getDate().toString().padStart(2,'00')
	}

	getSeconds(loginDate: String)
	{
		let t1 = new Date(loginDate.toString()),
			t2 = new Date(),
			dif = t1.getTime() - t2.getTime(),
			seg = dif / 1000,
			seconds = Math.abs(seg)
		return seconds
	}

	dtConfig(Obj: any)
	{
		const NewObj = Obj
		if (NewObj === null || NewObj === undefined )
        {
            return NewObj
		}
		else if(typeof NewObj !== 'object' )
        {
            return NewObj
		}
		else
		{
			for (const key of Object.keys(NewObj))
			{
				const value = NewObj[key]
				
				if (m.isDate(value))
				{
					NewObj[key] = m(value).subtract(6, 'hours')
				}
				else if(typeof value === 'object')
				{
					this.dtConfig(value)
				}
			}
			return NewObj
		}
		
	}

	convertExcelDate(sDate: any)
	{
		if (sDate === null || sDate === undefined )
        {
            return sDate
		}
		else if (typeof sDate !== 'object' )
        {
            return sDate
		}
		else
		{
			for (const key of Object.keys(sDate))
			{
				const value = sDate[key]
				if (this.isIsoDateString(value))
				{
					sDate[key] = new Date(value)
				}
				else if(typeof value === 'object')
				{
					this.convertExcelDate(value)
				}
			}
			return sDate
		}

	}

	isIsoDateString(value: any): boolean
    {
        if (value === null || value === undefined)
        {
            return false
        }
        if (typeof value === 'string')
        {
            return this.simpleFormat.test(value)
        }
        return false
    }

}
