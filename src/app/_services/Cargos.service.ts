import { Injectable, Inject } from '@angular/core'
import { Observable } from 'rxjs'
import { HttpClient, HttpHeaders, HttpParams, JsonpClientBackend } from '@angular/common/http'
import { map, filter, switchMap, catchError } from 'rxjs/operators'
import { getBaseUrl } from '../app.config'
import Swal from 'sweetalert2'
import { NgxSpinnerService } from 'ngx-spinner'
import { CargosModelo } from '../_models'

@Injectable({
	providedIn: 'root'
})

export class CargosService {

	private urlEndPoint: string = getBaseUrl()
	private httpOptionsPost = {
		headers: new HttpHeaders({
		  "Content-Type": "application/json"
		})
	}
	private httpOptionsPostFile = {
		headers: new HttpHeaders({
		  "Content-Type": "application/x-www-form-urlencoded"
		})
	}
	
	constructor(
		private _http: HttpClient,
		private _Spinner : NgxSpinnerService
	) { }

	async mostrarTodos()
	{
		try {
			const res = await this._http
						.get<CargosModelo[]>( this.urlEndPoint + 'cargos/mostrarTodos' )
						.toPromise()
						.then(resp => {
							return resp
						},
						pError => {
							Swal.fire(pError.statusText, pError.message, 'error')
							this._Spinner.hide()
						})
			const data = <CargosModelo[]>res
			return data

		} catch (error) {
			console.log(error)
		}
	}

	async crear(dataSend: CargosModelo)
	{
		try {
			const res = await this._http
						.post<CargosModelo>( this.urlEndPoint + 'cargos/crear', JSON.stringify(dataSend), this.httpOptionsPost )
						.toPromise()
						.then(resp => {
							return resp
						},
						pError => {
							console.log(pError)
							Swal.fire('Error', pError.message, 'error')
							this._Spinner.hide()
						})
			const data = <CargosModelo>res
			return data

		} catch (error) {
			console.log(error)
			Swal.fire('Error Catch', error, 'error')
		}
	}

	async actualizar(dataSend: CargosModelo)
	{
		try {
			const res = await this._http
						.post<CargosModelo>( this.urlEndPoint + 'cargos/actualizar', dataSend, this.httpOptionsPost )
						.toPromise()
						.then(resp => {
							return resp
						},
						pError => {
							Swal.fire(pError.statusText, pError.message, 'error')
							this._Spinner.hide()
						})
			const data = <CargosModelo>res
			return data

		} catch (error) {
			console.log(error)
		}
	}
}
