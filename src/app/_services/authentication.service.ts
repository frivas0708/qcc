﻿import { Injectable } from '@angular/core'
import { HttpClient, HttpHeaders } from '@angular/common/http'
import { BehaviorSubject, Observable, throwError } from 'rxjs'
import { map, catchError } from 'rxjs/operators'

import { environment } from '@environments/environment'
import { UsuariosModelo } from '@app/_models'
import { isNullOrUndefined } from 'util'

@Injectable({ providedIn: 'root' })
export class AuthenticationService {
    private currentUserSubject: BehaviorSubject<UsuariosModelo>
    public currentUser: Observable<UsuariosModelo>
    private httpHeaders: HttpHeaders = new HttpHeaders({'Content-Type': 'application/json'}) 
    
    UsersData	: UsuariosModelo[] = new Array<UsuariosModelo>()

    constructor(private http: HttpClient) {
        this.currentUserSubject = new BehaviorSubject<UsuariosModelo>(JSON.parse(sessionStorage.getItem('currentUser')))
        this.currentUser = this.currentUserSubject.asObservable()
    }

    public get currentUserValue(): UsuariosModelo {
        return this.currentUserSubject.value
    }

    login(username: string, password: string)
    {
        let data = { username, password }
        return this.http.post<any>(`${environment.apiUrl}auth/login`, data, {headers: this.httpHeaders})
        .pipe(
            map((resp: any) => {
				if (!isNullOrUndefined(resp.data))
				{
					this.UsersData = resp.data
				    sessionStorage.setItem('currentUser', JSON.stringify(this.UsersData[0]))
				    this.currentUserSubject.next(this.UsersData[0])
				}
				return resp
			}),
			catchError(err => {
				return throwError(err)
			})
        )
    }

    logout() {
        // remove user from local storage to log user out
        console.log(this.currentUserValue);
        let id = this.currentUserValue.id
        sessionStorage.removeItem('currentUser')
        this.currentUserSubject.next(null)
        
        return this.http.get<any>(`${environment.apiUrl}auth/logout/${id}`)
		.pipe(
			map((resp: any) => {
				return resp
			})
		)
    }
}