import { Injectable, Inject } from '@angular/core'
import { Observable } from 'rxjs'
import { HttpClient, HttpHeaders, HttpParams, JsonpClientBackend } from '@angular/common/http'
import { map, filter, switchMap, catchError } from 'rxjs/operators'
import { getBaseUrl } from '../app.config'
import Swal from 'sweetalert2'
import { NgxSpinnerService } from 'ngx-spinner'
import { TiposModelo } from '../_models'

@Injectable({
	providedIn: 'root'
})

export class TiposService {

	private urlEndPoint: string = getBaseUrl()
	private httpOptionsPost = {
		headers: new HttpHeaders({
		  "Content-Type": "application/json"
		})
	}
	private httpOptionsPostFile = {
		headers: new HttpHeaders({
		  "Content-Type": "application/x-www-form-urlencoded"
		})
	}
	
	constructor(
		private _http: HttpClient,
		private _Spinner : NgxSpinnerService
	) { }

	async mostrarTodos()
	{
		try {
			const res = await this._http
						.get<TiposModelo[]>( this.urlEndPoint + 'tipos/mostrarTodos' )
						.toPromise()
						.then(resp => {
							return resp
						},
						pError => {
							Swal.fire(pError.statusText, pError.message, 'error')
							this._Spinner.hide()
						})
			const data = <TiposModelo[]>res
			return data

		} catch (error) {
			console.log(error)
		}
	}

	async crear(dataSend: TiposModelo)
	{
		try {
			const res = await this._http
						.post<TiposModelo>( this.urlEndPoint + 'tipos/crear', JSON.stringify(dataSend), this.httpOptionsPost )
						.toPromise()
						.then(resp => {
							return resp
						},
						pError => {
							console.log(pError)
							Swal.fire('Error', pError.message, 'error')
							this._Spinner.hide()
						})
			const data = <TiposModelo>res
			return data

		} catch (error) {
			console.log(error)
			Swal.fire('Error Catch', error, 'error')
		}
	}

	async actualizar(dataSend: TiposModelo)
	{
		try {
			const res = await this._http
						.post<TiposModelo>( this.urlEndPoint + 'tipos/actualizar', dataSend, this.httpOptionsPost )
						.toPromise()
						.then(resp => {
							return resp
						},
						pError => {
							Swal.fire(pError.statusText, pError.message, 'error')
							this._Spinner.hide()
						})
			const data = <TiposModelo>res
			return data

		} catch (error) {
			console.log(error)
		}
	}
}
