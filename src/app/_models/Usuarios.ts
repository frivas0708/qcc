export class UsuariosModelo
{
	id					: number
	codigo				: string
	nombre				: string
	apellido			: string
	idperfil			: number
	perfil				: string
	usuario				: string
	clave				: string
	telefono1			: string
	telefono2			: string
	correo1				: string
	correo2				: string
	whatsapp			: string
	skype				: string
	token				: string
	fecha_creacion		: Date
	creado_por			: number
	fecha_modificacion	: Date
	modificado_por		: number
	activo				: boolean

	constructor()
	{
		this.id = -1
		this.codigo = ''
		this.nombre = ''
		this.apellido = ''
		this.idperfil = -1
		this.perfil = ''
		this.usuario = ''
		this.clave = ''
		this.telefono1 = ''
		this.telefono2 = ''
		this.correo1 = ''
		this.correo2 = ''
		this.whatsapp = ''
		this.skype = ''
		this.token = ''
		this.fecha_creacion = new Date(Date.now())
		this.creado_por = -1
		this.fecha_modificacion = new Date(Date.now())
		this.modificado_por = -1
		this.activo = true
	}	
}