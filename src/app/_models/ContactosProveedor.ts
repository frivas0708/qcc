export class ContactosProveedorModelo
{
    id : number
    idProveedor : number
    nombre : string
    idCargo : number
    telefono1 : number
    telefono2 : number
    correo1 : string
    correo2 : string
    whatsapp : number
    skype : string
    fecha_creacion : Date
    creado_por : number
    fecha_modificacion : Date
    modificado_por : number
    activo : boolean

	constructor()
	{
        this.id = -1
        this.idProveedor = -1
        this.nombre = ''
        this.idCargo = -1
        this.telefono1 = null
        this.telefono2 = null
        this.correo1 = ''
        this.correo2 = ''
        this.whatsapp = null
        this.skype = ''
        this.fecha_creacion = new Date()
        this.creado_por = null
        this.fecha_modificacion = new Date()
        this.modificado_por = null
        this.activo = true
	}	
}