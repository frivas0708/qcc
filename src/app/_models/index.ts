﻿export * from './role';
export * from './user';

export * from './Cargos';
export * from './Clientes';
export * from './ContactosClientes';
export * from './ContactosProveedor';
export * from './Departamentos';
export * from './Municipios';
export * from './Perfiles';
export * from './Proveedores';
export * from './Rubro';
export * from './SubRubro';
export * from './Tipos';
export * from './Usuarios';