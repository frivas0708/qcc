export class RubroModelo
{
    id : number
    codigo : string
    nombre : string
    fecha_creacion : Date
    creado_por : number
    fecha_modificacion : Date
    modificado_por : number
    activo : boolean

	constructor()
	{
		this.id = -1
        this.codigo = ''
        this.nombre = ''
        this.fecha_creacion = new Date()
        this.creado_por = null
        this.fecha_modificacion = new Date()
        this.modificado_por = null
        this.activo = true
	}	
}