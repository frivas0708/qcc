﻿import { Routes, RouterModule } from '@angular/router'

import { HomeComponent } from '@app/home'
import { AdminComponent } from '@app/admin'
import { LoginComponent } from '@app/login'
import { AuthGuard } from '@app/_helpers'
import { Role } from '@app/_models'

import { CargosComponent } from '@app/components/mantenimientos/cargos/cargos.component'
import { ListaClientesComponent } from '@app/components/clientes/lista-clientes/lista-clientes.component'
import { ListaUsuariosComponent } from '@app/components/usuarios/lista-usuarios/lista-usuarios.component'
import { ListaProveedoresComponent } from '@app/components/proveedores/lista-proveedores/lista-proveedores.component'
import { PerfilesComponent } from '@app/components/usuarios/perfiles/perfiles.component'
import { PermisosComponent } from '@app/components/usuarios/permisos/permisos.component'
import { RubroComponent } from '@app/components/mantenimientos/rubro/rubro.component'
import { SubrubroComponent } from '@app/components/mantenimientos/subrubro/subrubro.component'
import { TiposComponent } from '@app/components/mantenimientos/tipos/tipos.component'

const routes: Routes = [
    {
        path: '',
        component: HomeComponent,
        canActivate: [AuthGuard]
    },
    {
        path: 'admin',
        component: AdminComponent,
        canActivate: [AuthGuard],
        data: { roles: [Role.Admin] }
    },
    {
        path: 'login',
        component: LoginComponent
    },

    { path: 'Cargos', component: CargosComponent},
	{ path: 'ListaClientes', component: ListaClientesComponent},
	{ path: 'ListaUsuarios', component: ListaUsuariosComponent},
	{ path: 'ListaProveedores', component: ListaProveedoresComponent},
	{ path: 'Perfiles', component: PerfilesComponent},
	{ path: 'Permisos', component: PermisosComponent},
	{ path: 'Rubros', component: RubroComponent},
	{ path: 'SubRubros', component: SubrubroComponent},
	{ path: 'Tipos', component: TiposComponent},

    // otherwise redirect to home
    { path: '**', redirectTo: '' }
]

export const appRoutingModule = RouterModule.forRoot(routes)