import { Component, OnInit } from '@angular/core'
import { FormGroup, FormBuilder, FormControl, Validators, ValidationErrors } from '@angular/forms'
import { NgxSpinnerService } from "ngx-spinner"
import { ProveedoresModelo } from '@app/_models'
import { ProveedoresService } from '@app/_services'
import Swal from 'sweetalert2'
import { isNullOrUndefined } from 'util'
import { SelectItem } from 'primeng/api/selectitem'

@Component({
  selector: 'app-lista-proveedores',
  templateUrl: './lista-proveedores.component.html',
  styleUrls: ['./lista-proveedores.component.less']
})
export class ListaProveedoresComponent implements OnInit {
	Cols			: any = []
	spinnerText		: string
	DialogTitle		: string
	MostrarForm		: boolean = false
	Vacio			: boolean = false
	
	Lista			: ProveedoresModelo[] = new Array<ProveedoresModelo>()
	Objeto			: ProveedoresModelo = new ProveedoresModelo()
	formGroup		: FormGroup
	
	paginacionNum	: number = 15
	paginacionLista	: SelectItem[] = []

	constructor(
		private _Service : ProveedoresService,
		private _Spinner : NgxSpinnerService,
		private _FormBuilder : FormBuilder
	) { }

	ngOnInit()
	{
		this.Cols = [
			{ field: 'id', header: '#' },
			{ field: 'nombre', header: 'Nombre' },
			{ field: 'razonsocial', header: 'Razon Social' },
			{ field: 'vendedor', header: 'Vendedor' },
			{ field: 'fecha_creacion', header: 'Fecha Creación' }
		]

		this.paginacionLista.push(
			{label: 'Mostrar 15 registros', value: 15},
			{label: 'Mostrar 25 registros', value: 25},
			{label: 'Mostrar 50 registros', value: 50},
			{label: 'Mostrar 100 registros', value: 100}
		)

		this.obtener()
		this.construirFormulario()
	}

	construirFormulario()
	{
		this.formGroup = this._FormBuilder.group({
			id : new FormControl(this.Objeto.id),
			codigo : new FormControl(this.Objeto.codigo, Validators.required),
			nombre : new FormControl(this.Objeto.nombre, Validators.required),
			fecha_creacion : new FormControl(this.Objeto.fecha_creacion),
			creado_por : new FormControl(this.Objeto.creado_por),
			fecha_modificacion : new FormControl(this.Objeto.fecha_modificacion),
			modificado_por : new FormControl(this.Objeto.modificado_por),
			activo : new FormControl(this.Objeto.activo)
		})
	}

	get f() { return this.formGroup.controls; }

	obtener()
	{
		this.spinnerText = 'Obteniendo datos...'
		this._Spinner.show(undefined, {fullScreen:true})
		this._Service.mostrarTodos().then(data => {
			this.Lista = data
			this._Spinner.hide()
		},
		err => {
			this._Spinner.hide()
		})
	}

	nuevo()
	{
		this.formGroup.reset()
		this.DialogTitle = "Cargo Nuevo"
		this.MostrarForm = true
	}

	modificar(row)
	{
		let rowData = row
		this.formGroup.setValue(rowData)
		this.DialogTitle = "Modificar Cargo " + row.nombre
		this.MostrarForm = true
	}

	guardar()
	{
		if(this.formGroup.invalid)
		{
			this.Vacio = true
			Swal.fire('Información', 'Hay campos que son requeridos o con formato especifico, favor revisar.', 'info')
		}
		else
		{
			this.Objeto = this.formGroup.getRawValue()
			if(this.Objeto.id == 0 || isNullOrUndefined(this.Objeto.id))
			{
				this.agregar()
			}
			else
			{
				this.actualizar()
			}
		}

	}

	agregar()
	{
		if(this.formGroup.invalid)
		{
			this.Vacio = true
			Swal.fire('Información', 'Hay campos que son requeridos o con formato especifico, favor revisar.', 'info')
		}
		else
		{
			this.Vacio = false
			this.spinnerText = 'Creando registro...'
			this._Spinner.show(undefined, {fullScreen:true})
			this._Service.crear(this.Objeto).then(data =>{
				this.MostrarForm = false
				this._Spinner.hide()
				this.obtener()
			},
			err => {
				this._Spinner.hide()
			})
		}
	}

	actualizar()
	{
		this.spinnerText = 'Modificando registro...'
		this._Spinner.show(undefined, {fullScreen:true})
		this.Objeto = this.formGroup.getRawValue()
		this._Service.actualizar(this.Objeto).then(data =>{
			this.MostrarForm = false
			this._Spinner.hide()
			this.obtener()
		},
		err => {
			this._Spinner.hide()
		})
	}
	
	eliminar(row)
	{
		let _this = this

		Swal.fire({
			title: 'Eliminar',
			text: '¿Desea eliminar el cliente ' + row.nombre + '?',
			icon: 'warning',
			showCancelButton: true,
			confirmButtonColor: '#3085d6',
			cancelButtonColor: '#d33',
			confirmButtonText: 'Eliminar',
			cancelButtonText: 'Cancelar'
		}).then((result) => {
			if (result.value)
			{
				_this.Objeto = row
				_this.Objeto.activo = false
				_this.spinnerText = 'Eliminando registro...'
				_this._Spinner.show(undefined, {fullScreen:true})
				_this._Service.actualizar(_this.Objeto).then(data =>{
					_this._Spinner.hide()
					_this.obtener()
				},
				err => {
					_this._Spinner.hide()
				})
			}
		})
	}

	recargar()
	{
		this.obtener()
	}

	getFormValidationErrors()
	{
		Object.keys(this.formGroup.controls)
		.forEach(key => {
			const controlErrors: ValidationErrors = this.formGroup.get(key).errors;
			if (controlErrors != null) {
				Object.keys(controlErrors).forEach(keyError => {
				console.log('Key control: ' + key + ', keyError: ' + keyError + ', err value: ', controlErrors[keyError]);
				});
			}
		});
	}

}