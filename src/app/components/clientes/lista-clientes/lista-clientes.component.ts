import { Component, OnInit, ViewChild } from '@angular/core'
import { FormGroup, FormBuilder, FormControl, Validators, ValidationErrors } from '@angular/forms'
import { NgxSpinnerService } from "ngx-spinner"
import { ClientesModelo, TiposModelo, RubroModelo, SubRubroModelo, UsuariosModelo, CargosModelo, DepartamentosModelo, MunicipiosModelo } from '@app/_models'
import { ClientesService, TiposService, RubrosService, SubrubrosService, CargosService, UsuariosService, MunicipiosService } from '@app/_services'
import Swal from 'sweetalert2'
import { isNullOrUndefined } from 'util'
import { SelectItem } from 'primeng/api/selectitem'
import { DepartamentosService } from '@app/_services/Departamentos.service'

@Component({
	selector: 'app-lista-clientes',
	templateUrl: './lista-clientes.component.html',
	styleUrls: ['./lista-clientes.component.less']
})

export class ListaClientesComponent implements OnInit {
	Cols			: any = []
	spinnerText		: string
	DialogTitle		: string
	MostrarForm		: boolean = false
	Vacio			: boolean = false
	
	Lista			: ClientesModelo[] = new Array<ClientesModelo>()
	Objeto			: ClientesModelo = new ClientesModelo()
	formGroup		: FormGroup
	
	paginacionNum	: number = 15
	paginacionLista	: SelectItem[] = []

	Vendedores		: UsuariosModelo[] = Array<UsuariosModelo>()
	listaVendedores	: SelectItem[] = []
	Tipos			: TiposModelo[] = Array<TiposModelo>()
	listaTipos		: SelectItem[] = []
	Rubros			: RubroModelo[] = Array<RubroModelo>()
	listaRubros		: SelectItem[] = []
	SubRubros		: SubRubroModelo[] = Array<SubRubroModelo>()
	listaSubRubros	: SelectItem[] = []
	Cargos			: CargosModelo[] = Array<CargosModelo>()
	listaCargos		: SelectItem[] = []
	Deptos			: DepartamentosModelo[] = Array<DepartamentosModelo>()
	listaDeptos		: SelectItem[] = []
	Munics			: MunicipiosModelo[] = Array<MunicipiosModelo>()
	listaMunics		: SelectItem[] = []

	constructor(
		private _Service : ClientesService,
		private _Spinner : NgxSpinnerService,
		private _vendedorService : UsuariosService,
		private _tipoService : TiposService,
		private _rubroService : RubrosService,
		private _subrubroService : SubrubrosService,
		private _cargoService : CargosService,
		private _deptoService : DepartamentosService,
		private _municService : MunicipiosService,
		private _FormBuilder : FormBuilder
	) { }

	ngOnInit()
	{
		this.Cols = [
			{ field: 'id', header: '#' },
			{ field: 'nombre', header: 'Nombre' },
			{ field: 'razonsocial', header: 'Razon Social' },
			{ field: 'vendedor', header: 'Vendedor' },
			{ field: 'fecha_creacion', header: 'Fecha Creación' }
			
		]

		this.paginacionLista.push(
			{label: 'Mostrar 15 registros', value: 15},
			{label: 'Mostrar 25 registros', value: 25},
			{label: 'Mostrar 50 registros', value: 50},
			{label: 'Mostrar 100 registros', value: 100}
		)

		this.obtener()
		this.construirFormulario()
	}
	
	getAllData() //this._rubroService.mostrarTodos().
	{
		this.listaVendedores = []
		this.listaTipos = []
		this.listaRubros = []
		this.listaCargos = []
		this.listaDeptos = []

		this._Spinner.show()
		this._vendedorService.mostrarTodos().then(
			(vendedores)=> {
				this.Vendedores = vendedores.filter(fil=> fil.idperfil == 2)
				this.Vendedores.forEach(item=>{
					this.listaVendedores.push({ label: item.nombre + ' ' + item.apellido, value: item.id })
				})
			}
		).finally(
			()=>{
				this._tipoService.mostrarTodos().then(
					(tipos)=> {
						this.Tipos = tipos
						tipos.forEach(item=>{
							this.listaTipos.push({ label: item.nombre, value: item.id })
						})
					}
				).finally(
					()=>{
						this._rubroService.mostrarTodos().then(
							(rubros)=> {
								this.Rubros = rubros
								rubros.forEach(item=>{
									this.listaRubros.push({ label: item.nombre, value: item.id })
								})
							}
						).finally(
							()=>{

								this._cargoService.mostrarTodos().then(
									(cargos)=> {
										this.Cargos = cargos
										cargos.forEach(item=>{
											this.listaCargos.push({ label: item.nombre, value: item.id })
										})
									}
								).finally(
									()=>{
										this._deptoService.mostrarPorPais().then(
											(deptos)=> {
												this.Deptos = deptos
												deptos.forEach(item=>{
													this.listaDeptos.push({ label: item.nombre, value: item.id })
												})
											}
										).finally(
											()=>{
												this._Spinner.hide()
											}
										)
									}
								)
								
							}
						)
					}
				)
			}
		)

		
	}

	obtenerSubRubros(id:number)
	{
		this.listaSubRubros = []
		this.spinnerText = 'Obteniendo datos...'
		this._Spinner.show()
		this._subrubroService.mostrarPorRubro(id).then(
			(subrubros)=> {
				this.SubRubros = subrubros
				subrubros.forEach(item=>{
					this.listaSubRubros.push({ label: item.nombre, value: item.id })
				})
			}
		).finally(()=>{
			this._Spinner.hide()
			if(this.SubRubros.length>0)
			this.formGroup.controls.idSubrubro.enable()
		})
	}

	obtenerMunicipios(id:number)
	{
		this.listaMunics = []
		this.spinnerText = 'Obteniendo datos...'
		this._Spinner.show()
		this._municService.mostrarPorDepto(id).then(
			(munics)=> {
				this.Munics = munics
				munics.forEach(item=>{
					this.listaMunics.push({ label: item.nombre, value: item.id })
				})
			}
		).finally(()=>{
			this._Spinner.hide()
			this.formGroup.controls.idMunicipio.enable()
		})
	}

	construirFormulario()
	{
		this.formGroup = this._FormBuilder.group({
			id : new FormControl(this.Objeto.id),
			idVendedor : new FormControl(this.Objeto.idVendedor),
			idTipo : new FormControl(this.Objeto.idTipo),
			idRubro : new FormControl(this.Objeto.idRubro),
			idSubrubro : new FormControl({value: this.Objeto.idSubrubro, disabled: true}),
			codigo : new FormControl(this.Objeto.codigo),
			nombre : new FormControl(this.Objeto.nombre),
			razonsocial : new FormControl(this.Objeto.razonsocial),
			direccion : new FormControl(this.Objeto.direccion),
			idDepartamento : new FormControl(this.Objeto.idDepartamento),
			idMunicipio : new FormControl({value: this.Objeto.idMunicipio, disabled: true}),
			contacto : new FormControl(this.Objeto.contacto),
			idCargo : new FormControl(this.Objeto.idCargo),
			telefono1 : new FormControl(this.Objeto.telefono1),
			telefono2 : new FormControl(this.Objeto.telefono2),
			correo1 : new FormControl(this.Objeto.correo1),
			correo2 : new FormControl(this.Objeto.correo2),
			whatsapp : new FormControl(this.Objeto.whatsapp),
			skype : new FormControl(this.Objeto.skype),
			fecha_creacion : new FormControl(this.Objeto.fecha_creacion),
			creado_por : new FormControl(this.Objeto.creado_por),
			fecha_modificacion : new FormControl(this.Objeto.fecha_modificacion),
			modificado_por : new FormControl(this.Objeto.modificado_por),
			activo : new FormControl(this.Objeto.activo)
		})
	}

	get f() { return this.formGroup.controls; }

	obtener()
	{
		this.spinnerText = 'Obteniendo datos...'
		this._Spinner.show()
		this._Service.mostrarTodos().then(data => {
			this.Lista = data
		},
		err => {
			this._Spinner.hide()
		}).finally(()=>{
			this._Spinner.hide()
		})
	}

	nuevo()
	{
		this.formGroup.reset()
		this.DialogTitle = "Cargo Nuevo"
		this.getAllData()
		this.MostrarForm = true
	}

	modificar(row)
	{
		let rowData = row
		this.formGroup.setValue(rowData)
		this.DialogTitle = "Modificar Cargo " + row.nombre
		this.getAllData()
		this.MostrarForm = true
	}

	guardar()
	{
		if(this.formGroup.invalid)
		{
			this.Vacio = true
			Swal.fire('Información', 'Hay campos que son requeridos o con formato especifico, favor revisar.', 'info')
		}
		else
		{
			this.Objeto = this.formGroup.getRawValue()
			if(this.Objeto.id == 0 || isNullOrUndefined(this.Objeto.id))
			{
				this.agregar()
			}
			else
			{
				this.actualizar()
			}
		}

	}

	agregar()
	{
		if(this.formGroup.invalid)
		{
			this.Vacio = true
			Swal.fire('Información', 'Hay campos que son requeridos o con formato especifico, favor revisar.', 'info')
		}
		else
		{
			this.Vacio = false
			this.spinnerText = 'Creando registro...'
			this._Spinner.show(undefined, {fullScreen:true})
			this._Service.crear(this.Objeto).then(data =>{
				this.MostrarForm = false
				this._Spinner.hide()
				this.obtener()
			},
			err => {
				this._Spinner.hide()
			})
		}
	}

	actualizar()
	{
		this.spinnerText = 'Modificando registro...'
		this._Spinner.show(undefined, {fullScreen:true})
		this.Objeto = this.formGroup.getRawValue()
		this._Service.actualizar(this.Objeto).then(data =>{
			this.MostrarForm = false
			this._Spinner.hide()
			this.obtener()
		},
		err => {
			this._Spinner.hide()
		})
	}
	
	eliminar(row)
	{
		let _this = this

		Swal.fire({
			title: 'Eliminar',
			text: '¿Desea eliminar el cliente ' + row.nombre + '?',
			icon: 'warning',
			showCancelButton: true,
			confirmButtonColor: '#3085d6',
			cancelButtonColor: '#d33',
			confirmButtonText: 'Eliminar',
			cancelButtonText: 'Cancelar'
		}).then((result) => {
			if (result.value)
			{
				_this.Objeto = row
				_this.Objeto.activo = false
				_this.spinnerText = 'Eliminando registro...'
				_this._Spinner.show(undefined, {fullScreen:true})
				_this._Service.actualizar(_this.Objeto).then(data =>{
					_this._Spinner.hide()
					_this.obtener()
				},
				err => {
					_this._Spinner.hide()
				})
			}
		})
	}

	recargar()
	{
		this.obtener()
	}

	getFormValidationErrors()
	{
		Object.keys(this.formGroup.controls)
		.forEach(key => {
			const controlErrors: ValidationErrors = this.formGroup.get(key).errors;
			if (controlErrors != null) {
				Object.keys(controlErrors).forEach(keyError => {
				console.log('Key control: ' + key + ', keyError: ' + keyError + ', err value: ', controlErrors[keyError]);
				});
			}
		});
	}

}