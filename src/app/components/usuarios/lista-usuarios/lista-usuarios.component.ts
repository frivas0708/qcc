import { Component, OnInit } from '@angular/core'
import { FormGroup, FormBuilder, FormControl, Validators, ValidationErrors } from '@angular/forms'
import { NgxSpinnerService } from "ngx-spinner"
import { UsuariosModelo, PerfilesModelo } from '@app/_models'
import { UsuariosService, PerfilesService } from '@app/_services'
import Swal from 'sweetalert2'
import { MustMatch } from 'src/app/_helpers/mustmatch'
import { isNullOrUndefined } from 'util'
import { SelectItem } from 'primeng/api/selectitem'


@Component({
	selector: 'app-lista-usuarios',
	templateUrl: './lista-usuarios.component.html',
	styleUrls: ['./lista-usuarios.component.less']
})
export class ListaUsuariosComponent implements OnInit {

	Cols			: any = []
	spinnerText		: string
	DialogTitle		: string
	MostrarForm		: boolean = false
	Vacio			: boolean = false
	
	ListaUsuarios	: UsuariosModelo[] = new Array<UsuariosModelo>()
	Usuario			: UsuariosModelo = new UsuariosModelo()
	ListaPerfiles	: PerfilesModelo[] = new Array<PerfilesModelo>()
	ListaPerfilesCmb: SelectItem[] = []
	Perfil			: PerfilesModelo = new PerfilesModelo()
	fgUsuario		: FormGroup
	
	paginacionNum	: number = 15
	paginacionLista	: SelectItem[] = []

	constructor(
		private _UsuariosService : UsuariosService,
		private _PerfilesService : PerfilesService,
		private _Spinner : NgxSpinnerService,
		private _FormBuilder : FormBuilder
	) { }

	ngOnInit()
	{
		this.Cols = [
			{ field: 'id', header: '#' },
			{ field: 'nombre', header: 'Nombre' },
			{ field: 'apellido', header: 'Apellido' },
			{ field: 'perfil', header: 'Perfil' },
			{ field: 'correo1', header: 'Correo Electronico 1'},
			{ field: 'telefono1', header: 'Teléfono 1'},
			{ field: 'skype', header: 'Skype'}
		]

		this.paginacionLista.push(
			{label: 'Mostrar 15 registros', value: 15},
			{label: 'Mostrar 25 registros', value: 25},
			{label: 'Mostrar 50 registros', value: 50},
			{label: 'Mostrar 100 registros', value: 100}
		)

		this.getPerfiles()
		this.construirFormulario()
	}

	construirFormulario()
	{
		this.fgUsuario = this._FormBuilder.group({
			id : new FormControl(this.Usuario.id),
			codigo : new FormControl(this.Usuario.codigo, Validators.required),
			nombre : new FormControl(this.Usuario.nombre, Validators.required),
			apellido : new FormControl(this.Usuario.apellido, Validators.required),
			idperfil : new FormControl(this.Usuario.idperfil),
			perfil : new FormControl(),
			usuario : new FormControl(this.Usuario.usuario, Validators.required),
			clave : new FormControl(this.Usuario.clave, [Validators.required, Validators.minLength(8)]),
			claveConfirm : new FormControl(this.Usuario.clave, Validators.required),
			telefono1 : new FormControl(this.Usuario.telefono1),
			telefono2 : new FormControl(this.Usuario.telefono2),
			correo1 : new FormControl(this.Usuario.correo1, Validators.email),
			correo2 : new FormControl(this.Usuario.correo2, Validators.email),
			whatsapp : new FormControl(this.Usuario.whatsapp),
			skype : new FormControl(this.Usuario.skype),
			token : new FormControl(this.Usuario.token),
			fecha_creacion : new FormControl(this.Usuario.fecha_creacion),
			creado_por : new FormControl(this.Usuario.creado_por),
			fecha_modificacion : new FormControl(this.Usuario.fecha_modificacion),
			modificado_por : new FormControl(this.Usuario.modificado_por),
			activo : new FormControl(this.Usuario.activo)
		},{
			validators: MustMatch('clave', 'claveConfirm')
		})
	}

	get f() { return this.fgUsuario.controls; }

	getPerfiles()
	{
		this.spinnerText = 'Obteniendo datos...'
		this._Spinner.show(undefined, {fullScreen:true})
		this._PerfilesService.mostrarTodos().then(data => {
			this.ListaPerfiles = data
			data.forEach(f=>{
				this.ListaPerfilesCmb.push({ label: f.nombre, value: f.id })
			})
			this._Spinner.hide()
			this.getUsuarios()
		},
		err => {
			this._Spinner.hide()
		})
	}

	getUsuarios()
	{
		this.spinnerText = 'Obteniendo datos...'
		this._Spinner.show(undefined, {fullScreen:true})
		this._UsuariosService.mostrarTodos().then(data => {
			this.ListaUsuarios = data
			this.ListaUsuarios.forEach(f=>{
				let perfil = this.ListaPerfiles.filter( fil => fil.id == f.idperfil)
				if(perfil[0] !== undefined)
				{
					f.perfil = perfil[0].nombre
				}
			})
			this._Spinner.hide()
		},
		err => {
			this._Spinner.hide()
		})
	}

	nuevoUsuario()
	{
		this.fgUsuario.reset()
		this.DialogTitle = "Usuario Nuevo"
		this.MostrarForm = true
	}

	modificarUsuario(row)
	{
		let rowData = row
		rowData.claveConfirm = rowData.clave
		this.fgUsuario.setValue(rowData)
		this.DialogTitle = "Modificar Usuario " + row.usuario
		this.MostrarForm = true
	}

	guardar()
	{
		if(this.fgUsuario.invalid)
		{
			this.Vacio = true
			Swal.fire('Información', 'Hay campos que son requeridos o con formato especifico, favor revisar.', 'info')
		}
		else
		{
			this.Usuario = this.fgUsuario.getRawValue()
			if(this.Usuario.id == 0 || isNullOrUndefined(this.Usuario.id))
			{
				this.nuevo()
			}
			else
			{
				this.actualizar()
			}
		}

	}

	nuevo()
	{
		if(this.fgUsuario.invalid)
		{
			this.Vacio = true
			Swal.fire('Información', 'Hay campos que son requeridos o con formato especifico, favor revisar.', 'info')
		}
		else
		{
			this.Vacio = false
			this.spinnerText = 'Creando registro...'
			this._Spinner.show(undefined, {fullScreen:true})
			this._UsuariosService.crear(this.Usuario).then(data =>{
				this.MostrarForm = false
				this._Spinner.hide()
				//Swal.fire('Guardado', 'Se creo el registro para el usuario: ' + data[0].usuario, 'success')
			},
			err => {
				this._Spinner.hide()
			})
		}
	}

	actualizar()
	{
		this.spinnerText = 'Modificando registro...'
		this._Spinner.show(undefined, {fullScreen:true})
		this.Usuario = this.fgUsuario.getRawValue()
		this._UsuariosService.actualizar(this.Usuario).then(data =>{
			this.MostrarForm = false
			this._Spinner.hide()
			this.getUsuarios()
		},
		err => {
			this._Spinner.hide()
		})
	}
	
	eliminar(row)
	{
		let _this = this
		console.log(row);
		
		Swal.fire({
			title: 'Eliminar',
			text: '¿Desea eliminar el usuario ' + row.usuario + '?',
			icon: 'warning',
			showCancelButton: true,
			confirmButtonColor: '#3085d6',
			cancelButtonColor: '#d33',
			confirmButtonText: 'Eliminar',
			cancelButtonText: 'Cancelar'
		}).then((result) => {
			if (result.value)
			{
				_this.Usuario = row
				_this.Usuario.activo = false
				_this.spinnerText = 'Eliminando registro...'
				_this._Spinner.show(undefined, {fullScreen:true})
				_this._UsuariosService.actualizar(_this.Usuario).then(data =>{
					_this._Spinner.hide()
					_this.getUsuarios()
				},
				err => {
					_this._Spinner.hide()
				})
			}
		})
	}

	recargar()
	{
		this.getUsuarios()
	}

	getFormValidationErrors()
	{
		Object.keys(this.fgUsuario.controls)
		.forEach(key => {
			const controlErrors: ValidationErrors = this.fgUsuario.get(key).errors;
			if (controlErrors != null) {
				Object.keys(controlErrors).forEach(keyError => {
				console.log('Key control: ' + key + ', keyError: ' + keyError + ', err value: ', controlErrors[keyError]);
				});
			}
		});
	}

	async validateData(usr: UsuariosModelo)
	{
		let Usr = new UsuariosModelo()

		for (const elem in usr)
		{
			if (usr.hasOwnProperty(elem))
			{
				this.Usuario[elem] = ( !isNaN(usr[elem]) ) ? Number(usr[elem]) : (isNullOrUndefined(usr[elem])) ? Usr[elem] : usr[elem]
			}
		}
	}

}