import { Component, OnInit } from '@angular/core'
import { ValidationErrors, FormGroup, FormBuilder, FormControl, Validators } from '@angular/forms'
import { RubrosService, SubrubrosService } from '@app/_services'
import { SelectItem } from 'primeng/api/selectitem'
import { NgxSpinnerService } from 'ngx-spinner'
import Swal from 'sweetalert2'
import { isNullOrUndefined } from 'util'
import { RubroModelo, SubRubroModelo } from '@app/_models'

@Component({
	selector: 'app-subrubro',
	templateUrl: './subrubro.component.html',
	styleUrls: ['./subrubro.component.less']
})
export class SubrubroComponent implements OnInit {
	Cols			: any = []
	spinnerText		: string
	DialogTitle		: string
	MostrarForm		: boolean = false
	Vacio			: boolean = false
	
	Lista			: SubRubroModelo[] = new Array<SubRubroModelo>()
	Objeto			: SubRubroModelo = new SubRubroModelo()
	rowGroupMetadata: any
	ListaRubros		: SelectItem[] = []
	formGroup		: FormGroup
	
	paginacionNum	: number = 15
	paginacionLista	: SelectItem[] = []

	constructor(
		private _RubroService : RubrosService,
		private _Service : SubrubrosService,
		private _Spinner : NgxSpinnerService,
		private _FormBuilder : FormBuilder
	) { }

	ngOnInit()
	{
		this.Cols = [
			{ field: 'id', header: '#' },
			{ field: 'codigo', header: 'Codigo' },
			{ field: 'nombre', header: 'Nombre' }
		]

		this.paginacionLista.push(
			{label: 'Mostrar 15 registros', value: 15},
			{label: 'Mostrar 25 registros', value: 25},
			{label: 'Mostrar 50 registros', value: 50},
			{label: 'Mostrar 100 registros', value: 100}
		)

		this.obtenerRubros()
		this.construirFormulario()
	}

	construirFormulario()
	{
		this.formGroup = this._FormBuilder.group({
			id : new FormControl(this.Objeto.id),
			idRubro : new FormControl(this.Objeto.idRubro, Validators.required),
			codigo : new FormControl(this.Objeto.codigo, Validators.required),
			nombre : new FormControl(this.Objeto.nombre, Validators.required),
			fecha_creacion : new FormControl(this.Objeto.fecha_creacion),
			creado_por : new FormControl(this.Objeto.creado_por),
			fecha_modificacion : new FormControl(this.Objeto.fecha_modificacion),
			modificado_por : new FormControl(this.Objeto.modificado_por),
			activo : new FormControl(this.Objeto.activo)
		})
	}

	get f() { return this.formGroup.controls }
	
	obtenerRubros()
	{
		this.spinnerText = 'Obteniendo datos...'
		this._Spinner.show(undefined, {fullScreen:true})
		this._RubroService.mostrarTodos().then(data => {
			data.forEach(f=>{
				this.ListaRubros.push({ label: f.nombre, value: f.id })
			})
			this._Spinner.hide()
			this.obtener()
		},
		err => {
			this._Spinner.hide()
		})
	}

	obtener()
	{
		this.spinnerText = 'Obteniendo datos...'
		this._Spinner.show(undefined, {fullScreen:true})
		this._Service.mostrarTodos().then(data => {
			this.Lista = data
			this.Lista.forEach(fe=>{
				let rubro = this.ListaRubros.filter( fil => fil.value == fe.idRubro)
				if(rubro[0] !== undefined)
				{
					fe.rubro = rubro[0].label
				}
				this.updateRowGroupMetaData();
			})
			this._Spinner.hide()
		},
		err => {
			this._Spinner.hide()
		})
	}

	updateRowGroupMetaData() {
		this.rowGroupMetadata = {};
		if (this.Lista) {
			for (let i = 0; i < this.Lista.length; i++) {
				let rowData = this.Lista[i];
				let rubro = rowData.rubro;
				if (i == 0) {
					this.rowGroupMetadata[rubro] = { index: 0, size: 1 };
				}
				else {
					let previousRowData = this.Lista[i - 1];
					let previousRowGroup = previousRowData.rubro;
					if (rubro === previousRowGroup)
						this.rowGroupMetadata[rubro].size++;
					else
						this.rowGroupMetadata[rubro] = { index: i, size: 1 };
				}
			}
		}
	}

	nuevo()
	{
		this.formGroup.reset()
		this.DialogTitle = "Sub Rubro Nuevo"
		this.MostrarForm = true
	}

	modificar(row)
	{
		let rowData = row
		this.formGroup.setValue(rowData)
		this.DialogTitle = "Modificar Sub Rubro " + row.nombre
		this.MostrarForm = true
	}

	guardar()
	{
		if(this.formGroup.invalid)
		{
			this.Vacio = true
			Swal.fire('Información', 'Hay campos que son requeridos o con formato especifico, favor revisar.', 'info')
		}
		else
		{
			this.Objeto = this.formGroup.getRawValue()
			if(this.Objeto.id == 0 || isNullOrUndefined(this.Objeto.id))
			{
				this.agregar()
			}
			else
			{
				this.actualizar()
			}
		}

	}

	agregar()
	{
		if(this.formGroup.invalid)
		{
			this.Vacio = true
			Swal.fire('Información', 'Hay campos que son requeridos o con formato especifico, favor revisar.', 'info')
		}
		else
		{
			this.Vacio = false
			this.spinnerText = 'Creando registro...'
			this._Spinner.show(undefined, {fullScreen:true})
			this._Service.crear(this.Objeto).then(data =>{
				this.MostrarForm = false
				this._Spinner.hide()
				this.obtener()
			},
			err => {
				this._Spinner.hide()
			})
		}
	}

	actualizar()
	{
		this.spinnerText = 'Modificando registro...'
		this._Spinner.show(undefined, {fullScreen:true})
		this.Objeto = this.formGroup.getRawValue()
		delete this.Objeto.rubro
		this._Service.actualizar(this.Objeto).then(data =>{
			this.MostrarForm = false
			this._Spinner.hide()
			this.obtener()
		},
		err => {
			this._Spinner.hide()
		})
	}
	
	eliminar(row)
	{
		let _this = this

		Swal.fire({
			title: 'Eliminar',
			text: '¿Desea eliminar el sub rubro ' + row.nombre + '?',
			icon: 'warning',
			showCancelButton: true,
			confirmButtonColor: '#3085d6',
			cancelButtonColor: '#d33',
			confirmButtonText: 'Eliminar',
			cancelButtonText: 'Cancelar'
		}).then((result) => {
			if (result.value)
			{
				_this.Objeto = row
				_this.Objeto.activo = false
				_this.spinnerText = 'Eliminando registro...'
				_this._Spinner.show(undefined, {fullScreen:true})
				delete _this.Objeto.rubro
				_this._Service.actualizar(_this.Objeto).then(data =>{
					_this._Spinner.hide()
					_this.obtener()
				},
				err => {
					_this._Spinner.hide()
				})
			}
		})
	}

	recargar()
	{
		this.obtener()
	}

	getFormValidationErrors()
	{
		Object.keys(this.formGroup.controls)
		.forEach(key => {
			const controlErrors: ValidationErrors = this.formGroup.get(key).errors
			if (controlErrors != null) {
				Object.keys(controlErrors).forEach(keyError => {
				console.log('Key control: ' + key + ', keyError: ' + keyError + ', err value: ', controlErrors[keyError])
				})
			}
		})
	}

}