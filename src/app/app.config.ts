import { environment } from '@environments/environment';

export function getBaseUrl() {
    return environment.apiUrl;
}
export function UrlReportViewer() {
    return environment.rptUrl;
}
