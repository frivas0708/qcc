﻿import { Component, OnInit, HostListener } from '@angular/core'
import { Router } from '@angular/router'

import { AuthenticationService, EventEmitterService, UsuariosService } from '@app/_services'
import { UsuariosModelo, Role } from '@app/_models'
import Swal from 'sweetalert2'
import { isNullOrUndefined } from 'util'
import { NgxSpinnerService } from 'ngx-spinner'
import { Subject } from 'rxjs'

@Component({
	selector: 'app',
	templateUrl: 'app.component.html'
})
export class AppComponent implements OnInit
{
	userActivity;
	userInactive: Subject<any> = new Subject();
	currentUser: UsuariosModelo
	spinnerText: string
	nombreApellido: string = ''

	constructor(
		private authenticationService: AuthenticationService,
		private eventEmitterService: EventEmitterService,
		private router: Router,
		private _Spinner: NgxSpinnerService,
		private usuariosService: UsuariosService
	) {
		this.authenticationService.currentUser.subscribe(x => this.currentUser = x);
		this.setTimeout();
		this.userInactive.subscribe(() => this.AddInactivity() )
	}
	
	ngOnInit()
	{
		if(this.currentUser)
		{
			this.initFn();
		}

		if (this.eventEmitterService.subsVar==undefined) {    
		  this.eventEmitterService.subsVar = this.eventEmitterService.initFunction.subscribe((name:string) => {    
			this.initFn();    
		  });    
		}    
	} 
	
	initFn()
	{
		this.nombreApellido = this.currentUser.nombre + ' ' + this.currentUser.apellido
	}
	
	get isAdmin() {
		return this.currentUser && this.currentUser.perfil === Role.Admin
	}

	setTimeout()
	{
		this.userActivity = setTimeout(() => this.userInactive.next(undefined), 300000);
	}
	
	@HostListener('window:mousemove') refreshUserState()
	{
		clearTimeout(this.userActivity);
		this.setTimeout();
	}

	AddInactivity()
	{
		//this._AgentService.changingState()
	}

	logout()
	{
		let _this = this
		Swal.fire({
			title : '¿Salir de la Sesión?',
			text : '¡Desea salir completamente de la sesión!',
			icon : 'question',
			showCancelButton : true,
			confirmButtonText : 'Si',
			cancelButtonText : 'No',
			confirmButtonColor : '#3085D6',
			cancelButtonColor : '#DD3333'
		}).then(function (result) {
			if (result.value) {
				_this._Spinner.show()
				_this.spinnerText = 'Cerrando Sesión'
				_this.authenticationService.logout().subscribe(
					(data) => {},
					(err: any) => {},
					() => {
						_this.router.navigate(['/login']);
						_this._Spinner.hide()
					}
				)
			}
		})
	}

	goTo(module)
	{
		this.router.navigate(["/"+ module])
	}
	
}