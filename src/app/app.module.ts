﻿import { BrowserAnimationsModule } from '@angular/platform-browser/animations'
import { BrowserModule } from '@angular/platform-browser'
import { CdTimerModule } from 'angular-cd-timer'
import { CommonModule } from '@angular/common'
import { CKEditorModule } from '@ckeditor/ckeditor5-angular';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http'
import { NgModule } from '@angular/core'
import { NgxSpinnerModule } from "ngx-spinner"
import { ReactiveFormsModule } from '@angular/forms'

/**
 * LTE COMPONENTS
 */
import { AppfooterComponent } from './appfooter/appfooter.component'
import { AppsettingComponent } from './appsetting/appsetting.component'

/**
 * PRIME NG MODULES
 */
import { AccordionModule } from "primeng/accordion"
import { AutoCompleteModule } from "primeng/autocomplete"
import { BreadcrumbModule } from "primeng/breadcrumb"
import { ButtonModule } from "primeng/button"
import { CalendarModule } from "primeng/calendar"
import { CardModule } from "primeng/card"
import { CarouselModule } from "primeng/carousel"
import { ChartModule } from "primeng/chart"
import { CheckboxModule } from "primeng/checkbox"
import { ChipsModule } from "primeng/chips"
import { CodeHighlighterModule } from "primeng/codehighlighter"
import { ConfirmDialogModule } from "primeng/confirmdialog"
import { ColorPickerModule } from "primeng/colorpicker"
import { ContextMenuModule } from "primeng/contextmenu"
import { DataViewModule } from "primeng/dataview"
import { DialogModule } from "primeng/dialog"
import { DropdownModule } from "primeng/dropdown"
import { EditorModule } from "primeng/editor"
import { FieldsetModule } from "primeng/fieldset"
import { FileUploadModule } from "primeng/fileupload"
import { GalleriaModule } from "primeng/galleria"
import { InplaceModule } from "primeng/inplace"
import { InputMaskModule } from "primeng/inputmask"
import { InputSwitchModule } from "primeng/inputswitch"
import { InputTextModule } from "primeng/inputtext"
import { InputTextareaModule } from "primeng/inputtextarea"
import { LightboxModule } from "primeng/lightbox"
import { ListboxModule } from "primeng/listbox"
import { MegaMenuModule } from "primeng/megamenu"
import { MenuModule } from "primeng/menu"
import { MenubarModule } from "primeng/menubar"
import { MessagesModule } from "primeng/messages"
import { MessageModule } from "primeng/message"
import { MultiSelectModule } from "primeng/multiselect"
import { OrderListModule } from "primeng/orderlist"
import { OrganizationChartModule } from "primeng/organizationchart"
import { OverlayPanelModule } from "primeng/overlaypanel"
import { PaginatorModule } from "primeng/paginator"
import { PanelModule } from "primeng/panel"
import { PanelMenuModule } from "primeng/panelmenu"
import { PasswordModule } from "primeng/password"
import { PickListModule } from "primeng/picklist"
import { ProgressBarModule } from "primeng/progressbar"
import { RadioButtonModule } from "primeng/radiobutton"
import { RatingModule } from "primeng/rating"
//import { ScheduleModule } from "primeng/schedule/schedule"
import { ScrollPanelModule } from "primeng/scrollpanel"
import { SelectButtonModule } from "primeng/selectbutton"
import { SidebarModule } from 'primeng/sidebar'
import { SlideMenuModule } from "primeng/slidemenu"
import { SliderModule } from "primeng/slider"
import { SpinnerModule } from "primeng/spinner"
import { SplitButtonModule } from "primeng/splitbutton"
import { StepsModule } from "primeng/steps"
import { TabMenuModule } from "primeng/tabmenu"
import { TableModule } from "primeng/table"
import { TabViewModule } from "primeng/tabview"
import { TerminalModule } from "primeng/terminal"
import { TieredMenuModule } from "primeng/tieredmenu"
import { ToastModule } from "primeng/toast"
import { ToggleButtonModule } from "primeng/togglebutton"
import { ToolbarModule } from "primeng/toolbar"
import { TooltipModule } from "primeng/tooltip"
import { TreeModule } from "primeng/tree"
import { TreeTableModule } from "primeng/treetable"
import { KeyFilterModule } from "primeng/keyfilter"
import { DynamicDialogModule } from "primeng/dynamicdialog"

// used to create fake backend
// import { fakeBackendProvider } from './_helpers';

import { AppComponent } from './app.component';
import { appRoutingModule } from './app.routing';

import { JwtInterceptor, ErrorInterceptor } from './_helpers';
import { HomeComponent } from './home';
import { AdminComponent } from './admin';
import { LoginComponent } from './login';

import { CargosComponent } from './components/mantenimientos/cargos/cargos.component'
import { ListaClientesComponent } from './components/clientes/lista-clientes/lista-clientes.component';
import { ListaUsuariosComponent } from './components/usuarios/lista-usuarios/lista-usuarios.component'
import { ListaProveedoresComponent } from './components/proveedores/lista-proveedores/lista-proveedores.component';
import { PerfilesComponent } from './components/usuarios/perfiles/perfiles.component';
import { PermisosComponent } from './components/usuarios/permisos/permisos.component';
import { RubroComponent } from './components/mantenimientos/rubro/rubro.component';
import { SubrubroComponent } from './components/mantenimientos/subrubro/subrubro.component';
import { TiposComponent } from './components/mantenimientos/tipos/tipos.component';
import { EventEmitterService } from './_services/event-emitter.service';

@NgModule({
    imports: [
        appRoutingModule,
		BrowserModule,
		BrowserAnimationsModule,
		CdTimerModule,
		CKEditorModule,
		CommonModule,
		HttpClientModule,
		NgxSpinnerModule,
		ReactiveFormsModule,


        /**
         * PRIMENG MODULES
         */
		AccordionModule,
		AutoCompleteModule,
		BreadcrumbModule,
		ButtonModule,
		CalendarModule,
		CardModule,
		CarouselModule,
		ChartModule,
		CheckboxModule,
		ChipsModule,
		CodeHighlighterModule,
		ConfirmDialogModule,
		ColorPickerModule,
		ContextMenuModule,
		DataViewModule,
		DialogModule,
		DropdownModule,
		EditorModule,
		FieldsetModule,
		FileUploadModule,
		GalleriaModule,
		InplaceModule,
		InputMaskModule,
		InputSwitchModule,
		InputTextModule,
		InputTextareaModule,
		LightboxModule,
		ListboxModule,
		MegaMenuModule,
		MenuModule,
		MenubarModule,
		MessagesModule,
		MessageModule,
		MultiSelectModule,
		OrderListModule,
		OrganizationChartModule,
		OverlayPanelModule,
		PaginatorModule,
		PanelModule,
		PanelMenuModule,
		PasswordModule,
		PickListModule,
		ProgressBarModule,
		RadioButtonModule,
		RatingModule,
		//ScheduleModule,
		ScrollPanelModule,
		SelectButtonModule,
		SidebarModule,
		SlideMenuModule,
		SliderModule,
		SpinnerModule,
		SplitButtonModule,
		StepsModule,
		TabMenuModule,
		TableModule,
		TabViewModule,
		TerminalModule,
		TieredMenuModule,
		ToastModule,
		ToggleButtonModule,
		ToolbarModule,
		TooltipModule,
		TreeModule,
		TreeTableModule,
		KeyFilterModule,
		DynamicDialogModule

    ],
    declarations: [

        /**
         * LTE COMPONENTS
         */
		AppfooterComponent,
		AppsettingComponent,

        AppComponent,
        HomeComponent,
        AdminComponent,
		LoginComponent,
		
		CargosComponent,
		ListaClientesComponent,
		ListaUsuariosComponent,
		ListaProveedoresComponent,
		PerfilesComponent,
		PermisosComponent,
		SubrubroComponent,
		RubroComponent,
		TiposComponent	
    ],
    providers: [
		EventEmitterService,
        { provide: HTTP_INTERCEPTORS, useClass: JwtInterceptor, multi: true },
        { provide: HTTP_INTERCEPTORS, useClass: ErrorInterceptor, multi: true }

        // provider used to create fake backend
        // fakeBackendProvider
    ],
    bootstrap: [AppComponent]
})

export class AppModule { }