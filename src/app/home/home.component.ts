﻿import { Component } from '@angular/core';
import { first } from 'rxjs/operators';

import { UsuariosModelo } from '@app/_models';
import { UserService, AuthenticationService, UsuariosService } from '@app/_services';

@Component({ templateUrl: 'home.component.html' })
export class HomeComponent {
    loading = false;
    currentUser: UsuariosModelo;
    userFromApi: UsuariosModelo;

    constructor(
        private usuarioService: UsuariosService,
        private authenticationService: AuthenticationService
    ) {
        this.currentUser = this.authenticationService.currentUserValue;
    }

    ngOnInit() {
        this.loading = true;
        this.usuarioService.mostrarPorId(this.currentUser.id).then(user=>{
            this.loading = false;
            this.userFromApi = user;
        })
    }
}