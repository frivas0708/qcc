﻿import { Component, OnInit } from '@angular/core'
import { Router, ActivatedRoute } from '@angular/router'
import { FormBuilder, FormGroup, Validators } from '@angular/forms'

import { AuthenticationService, EventEmitterService } from '@app/_services'
import { NgxSpinnerService } from 'ngx-spinner'
import Swal from 'sweetalert2'

@Component({
	templateUrl: 'login.component.html',
	styleUrls: ['login.component.less']
})
export class LoginComponent implements OnInit {
	loginForm: FormGroup
	loading = false
	submitted = false
	returnUrl: string
	error = ''
	spinnerText: string

	constructor(
		private formBuilder: FormBuilder,
		private _spinner: NgxSpinnerService,
		private route: ActivatedRoute,
		private router: Router,
		private eventEmitterService: EventEmitterService,
		private authenticationService: AuthenticationService
	) { 
		// redirect to home if already logged in
		if (this.authenticationService.currentUserValue) { 
			this.router.navigate(['/'])
		}
	}

	ngOnInit() {
		this.loginForm = this.formBuilder.group({
			username: ['', Validators.required],
			password: ['', Validators.required]
		})

		// get return url from route parameters or default to '/'
		this.returnUrl = this.route.snapshot.queryParams['returnUrl'] || '/'
	}

	// convenience getter for easy access to form fields
	get f() { return this.loginForm.controls }

	onSubmit() {
		this.error = ''
		this.submitted = true

		// stop here if form is invalid
		if (this.loginForm.invalid) {
			return
		}

		this.loading = true
		this._spinner.show()
		this.spinnerText = 'Iniciando Sesión'
		this.authenticationService.login(this.f.username.value, this.f.password.value)
		.subscribe(
			data => {
				if(data.success)
					{
						this.eventEmitterService.initFunctionEmmitter();
						this.router.navigate([this.returnUrl]);
					}
					else
					{
						Swal.fire("Inicio de Sesión",data.msg,"warning")
						this.loading = false;
					}
			},
			error => {
				this.error = error
				this.loading = false
				this._spinner.hide()
			},
			() => {
				this._spinner.hide()
			}
		)
	}
}
